function Enable-SMBv1 {
<#
.SYNOPSIS
    Enables the SMBv1 server/protocal

.PARAMETER NoRestart
    This will tell the command to skip needed reboots.
    Default is true specify $false to overwrite.

.NOTES
    Version:        1.0
    Author:         Chris Taylor
    Creation Date:  5/16/2017
    Purpose/Change: Initial script development

.LINK
  https://support.microsoft.com/en-us/help/2696547/how-to-enable-and-disable-smbv1,-smbv2,-and-smbv3-in-windows-vista,-windows-server-2008,-windows-7,-windows-server-2008-r2,-windows-8,-and-windows-server-2012

.EXAMPLE
    Enable-SMBv1 -NoRestart:$False
    This will enable SMBv1 and reboot if needed.
  
#>
    [CmdletBinding()]
    Param (
        [switch]$NoRestart = $true
    )
    
    Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Services\LanmanServer\Parameters" SMB1 -Type DWORD -Value 1 -Force -ErrorAction SilentlyContinue

    if([System.Environment]::OSVersion.Version -ge [System.Version]6.3){
        #install SMBv1
        if((Get-WmiObject -class Win32_OperatingSystem).Name -like '*server*'){
            if($NoRestart){
                $Install = Install-WindowsFeature FS-SMB1 -Confirm:$false
            }
            else{
                $Install = Install-WindowsFeature FS-SMB1 -Confirm:$false -Restart
            }                
        }
        else{
            $Install = Enable-WindowsOptionalFeature -Online -FeatureName smb1protocol -NoRestart:$NoRestart
        }
        Set-SmbServerConfiguration -EnableSMB1Protocol $true -Force -Confirm:$False
    }
    
    #Make sure that service is using the correct account.
    if([System.Environment]::OSVersion.Version -gt [System.Version]6.1)
    {
  
        $service = gwmi win32_service -filter "name='LanmanWorkstation'"
        $service.Change($null,$null,$null,$null,$null,$null,"NT AUTHORITY\NetworkService","",$null,$null,$null) | Out-Null
    }

    if([System.Environment]::OSVersion.Version -lt [System.Version]6.1 -and [System.Environment]::OSVersion.Version -gt [System.Version]6.0)
    {
  
        $service = gwmi win32_service -filter "name='LanmanWorkstation'"
        $service.Change($null,$null,$null,$null,$null,$null,"NT AUTHORITY\LocalService","",$null,$null,$null) | Out-Null
    }

    #Still not sure this will work for all OS's
    if([System.Environment]::OSVersion.Version -gt [System.Version]5.2){
        sc.exe config lanmanworkstation depend= bowser/mrxsmb10/mrxsmb20/nsi | Out-Null
        sc.exe config mrxsmb20 start= auto | Out-Null
        $Output = "Enablind with SMBv2 and all dependants"
        Stop-Service workstation -ErrorAction SilentlyContinue
        Start-Service workstation -ErrorAction SilentlyContinue
        if((Get-Service workstation).Status -ne 'Running'){
            $Output = "Enablind with SMBv1 and all dependants"
            sc.exe config lanmanworkstation depend= bowser/mrxsmb10/nsi
            sc.exe config mrxsmb10 start= auto | Out-Null
            Start-Service workstation -ErrorAction SilentlyContinue
        }
        if((Get-Service workstation).Status -ne 'Running'){
            $Output = "Enablind with SMBv1 with no dependant"
            sc.exe config lanmanworkstation depend= / | Out-Null
            sc.exe config mrxsmb10 start= auto | Out-Null
            try{
                Start-Service workstation
            }
            catch{
                Write-Output "There was an issue starting the workstation service with no dependancies."
                Exit
            }
        }
    }
    else{
        $Output = "Enablind with SMBv1 with no dependant (XP/2003)"
        sc.exe config lanmanworkstation depend= /
        sc.exe config mrxsmb10 start= auto
        try{
            Start-Service workstation
        }
        catch{
            Write-Output "There was an issue starting the workstation service with no dependancies."
            Exit
        }    
    }
    Write-Output $Output
    Start-Sleep -Seconds 5
    Start-Service netlogon
}

function Disable-SMBv1 {
<#
.SYNOPSIS
    Disables the SMBv1 server/protocal

.PARAMETER NoRestart
    This will tell the command to skip needed reboots.
    Default is true specify $false to overwrite.

.NOTES
    Version:        1.0
    Author:         Chris Taylor
    Creation Date:  5/16/2017
    Purpose/Change: Initial script development

.LINK
    https://support.microsoft.com/en-us/help/2696547/how-to-enable-and-disable-smbv1,-smbv2,-and-smbv3-in-windows-vista,-windows-server-2008,-windows-7,-windows-server-2008-r2,-windows-8,-and-windows-server-2012  

.EXAMPLE
    Disable-SMBv1 -NoRestart:$False
    This will Disable SMBv1 and reboot if needed.
#>
[CmdletBinding()]
Param (
    [switch]$NoRestart = $true
)

    if([System.Environment]::OSVersion.Version -ge 6.3){
        Set-SmbServerConfiguration -EnableSMB1Protocol $false -EnableSMB2Protocol $true -Confirm:$False -ErrorAction SilentlyContinue -Force

        #Uninstall SMBv1
        if((Get-WmiObject -class Win32_OperatingSystem).Name -like '*server*'){
                if($NoRestart){
                    Remove-WindowsFeature FS-SMB1 -ErrorAction SilentlyContinue | Out-Null
                }
                else{
                    Remove-WindowsFeature FS-SMB1 -ErrorAction SilentlyContinue -Restart | Out-Null 
                }
        }
        else{
            Disable-WindowsOptionalFeature -Online -FeatureName smb1protocol -NoRestart:$NoRestart -ErrorAction SilentlyContinue | Out-Null
        }
    }
    elseif([System.Environment]::OSVersion.Version -ge 5.5){
        Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Services\LanmanServer\Parameters" SMB1 -Type DWORD -Value 0 -Force -ErrorAction SilentlyContinue
        Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Services\LanmanServer\Parameters" SMB2 -Type DWORD -Value 1 -Force -ErrorAction SilentlyContinue
    }

    if([System.Environment]::OSVersion.Version -ge 5.5){
        sc.exe config lanmanworkstation depend= bowser/mrxsmb20/nsi
        sc.exe config mrxsmb10 start= disabled
        sc.exe config mrxsmb20 start= auto
    }
    
    Start-Service workstation
    Start-Service netlogon
}