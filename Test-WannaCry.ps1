<#
.SYNOPSIS
    This script will check if you machine is Secured/Vulnerable to the WannaCry vulnerability.

.DESCRIPTION
    This script will search windows update history for kb installs.
    It will also use Get-Hotfix to get a list of updates.
    Then compare them to a list of KB's and updates that patch teh vuln.

.PARAMETER KBs
    Gives you the ability to search for your own custom list of patches.
    Just supply a regex match string. 
    KB($KBs)

.NOTES
    Version:        1.0
    Author:         Darren White
                    Chris Taylor
    Creation Date:  5/16/2017
    Purpose/Change: Fork from, MVer6.1

.LINK
  http://www.labtechgeek.com/forum/viewtopic.php?f=3&t=3469&p=21352&hilit=wannacry#p21352
#>
[CmdletBinding()]
Param (
    $KBs
)

if([System.Environment]::OSVersion.Version -ge [System.Version]'10.0.15063'){
    Write-Output "Secured - Detectected Win10, 10.0.15063+"
    return
}

$tpvalue='401221[2-8]|4012598|4012606|4013198|4013429|4015217|4015438|401554[69]|401555[02]|4016635|4019215|401926[34]|4019472|402271[49]|402272[46]'

Set-Service wuauserv -StartupType automatic
Start-service wuauserv 

$Session = New-Object -ComObject 'Microsoft.Update.Session'
$Searcher = $Session.CreateUpdateSearcher()
$FormatEnumerationLimit=-1
try{
    $historyCount = $Searcher.GetTotalHistoryCount()
}
catch{
    Write-Verbose "There was an error getting history count. $($Error[0])"
}

if ($historyCount -gt 0) {
    Write-Verbose 'WU Patch History Available - Selecting History of patches installed, limited to operation "Installation" and resultcode "InProgress, Succeeded and SucceededWithErrors"'
    Write-Verbose 'Limiting final results to only include the Title'
    $wuKBLIST = $($Searcher.QueryHistory(0, $historyCount)|Select-Object Title, Date, Operation, Resultcode|Where-Object {$_.Operation -like 1 -and $_.Resultcode -match '[123]'}| Select-object -ExpandProperty Title)
}

try{
    $ghfKBLIST = $(Get-Hotfix -ErrorAction SilentlyContinue | Where-object {$_.hotfixid -match 'KB\d{6,7}'} | Select-object -ExpandProperty Hotfixid)
}
catch{
    Write-Verbose "There was an error with Get-Hotfix. $($Error[0])"
}

If ($wuKBLIST -eq $null -and $ghfKBLIST -eq $null) {
    Write-Warning 'No updates returned - Get-Hotfix and Windows Update History both returned no usable data'
    break
} 
else {
    Write-Verbose 'Filtering $results from above, containing either only Update Titles, or HotfixIDs'
    $finalKBLIST = $wuKBLIST + $ghfKBLIST  |
        Where-Object {$_ -match 'KB('+$tpvalue+')' -or 
            ($_ -match '^((2017-0[3-9]|2017-1[0-2]|201[89]-[0-9]{2})|(Ma|A|Ju|[SOND][^ ]+ber).* 2017 |[a-z]{3,10} 201[89] )' -and 
          $_  -match '(Security .*Rollup|Cumulative Update) for Windows')
        }
    if($KBs){
        $finalKBLIST += $wuKBLIST + $ghfKBLIST  |
            Where-Object {$_ -match 'KB('+$KBs+')'}
    }

    If ($finalKBLIST -eq $null) {
        Write-Output 'Vulnerable - Couldnt make a regex match or a name match'
    } 
    else {
        Write-Output "Secured - Detected Updates: $(($finalKBLIST | Select-String 'KB\d{6,7}' -AllMatches | ForEach-Object {$_.matches} | ForEach-Object {$_.Value} | Select-Object -Unique) -join ',')"
    }
}
